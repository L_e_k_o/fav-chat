;(function(global, $, undefined) {
	'use strict';

	/**
	 * @class ChatworkExtension
	 */
	// global.ChatworkExtension = (function() {
	var ChatworkExtension = (function() {
		var config = {
			ids: {
				roomInfo: {
					name: '_roomInfoName',
					description: '_roomInfoDescription'
				}
			},
			classes: {
				room: {
					el: '_room',
					title: 'chatListTitleArea',
					icon: 'roomIcon',
					pin: {
						el: '_pin',
						on: 'ico19PinOn',
						off: 'ico19PinOff',
					},
					selected: '_roomSelected'
				},
				message: {
					el: '_message',
					mine: 'chatTimeLineMessageMine',
					speaker: '_speaker',
					speakerName: '_speakerName',
					orgname: 'chatNameOrgname',
					timestamp: '_timeStamp'
				},
				menu: {
					message: {
						box: '_cwABAction linkStatus',
						text: '_showAreaText showAreatext'
					}
				}
			}
		};

		var $emoticonGallery = $("#_emoticonGallery img"),
			$timeline = $('#_timeLine'),
			$chat     = $('#_chatText'),
			$send     = $('#_sendButton'),
			$roomListBox = $('#_roomListItems'),
			$roomList = $roomListBox.find('li._roomLink'),
			$roomInfoIconPreset = $("#_roomInfoIconPreset img"),
			$notificationBox,
			ready = false;

		// 通知用の箱を作成
		$notificationBox = $('<div></div>').css({
			position: 'absolute',
			left: 30,
			top: 30,
			'z-index': 999999
		}).appendTo('body');

		/**
		 * チャットルームが追加された際のコールバック
		 * $roomListの更新とchatRoomsの更新を行う
		 */
		var roomObserver = new DOMObserver($roomListBox.get(0), { childList: true }),
			chatObserver = new DOMObserver($timeline.get(0), { childList: true });

		// 初回の１回をreadyとみなす
		roomObserver.once('add', function() {
			setTimeout(function() { ready = true; }, 1000);
		});

		function generateMessageMenu(name, icon) {
			var $menuBox = $('<li></li>'),
				$menu    = $('<span></span>');

			// メニュー用のHTMLを作成
			$menu.text(name);
			$menu.addClass(config.classes.menu.message.text);
			$menuBox.attr('role', 'button');
			$menuBox.addClass(config.classes.menu.message.box);

			if(typeof icon !== 'undefined') {
				var $icon = $('<span></span>').addClass(icon);
				$menuBox.append($icon);
			}
			$menuBox.append($menu);

			return $menuBox;
		}

		/**
		 * 一覧にあるチャットルーム１つをパースする
		 */
		function parseRoom($el) {
			var $root, icon, data;
			if($el.is('.' + config.classes.room.el)) {
				$root = $el;
			} else {
				$root = $el.closest('.' + config.classes.room.el);
			}

			var $icon = $root.find('.' + config.classes.room.icon + ' img');
			icon = $icon.attr('src').match(/ico_(\w+)\.png/)[1];

			data = {
				rid: $root.data('rid'),
				icon: icon,
				title: $root.find('.' + config.classes.room.title).text(),
				pinned: ($root.find('.' + config.classes.room.pin.on).size() > 0)
			};

			return data;
		}

		/**
		 * メッセージをパースする
		 * メッセージのメニューがクリックされて、そのメッセージの情報を修得することを想定している
		 */
		function parseMessage($el) {
			var $root;
			if($el.is('.' + config.classes.message.el)) {
				$root = $el;
			} else {
				$root = $el.closest('.' + config.classes.message.el);
			}

			var time  = +$root.find('.' + config.classes.message.timestamp).data('tm'),
				data = {
					'$root': $root,
					rid: $root.data('rid'),
					mid: $root.data('mid'),
					aid: $root.find('.' + config.classes.message.speaker + ' img').data('aid'),
					mine: $root.hasClass(config.classes.message.mine),
					name: $root.find('.' + config.classes.message.speakerName + ' span').text(),
					orgname: $root.find('.' + config.classes.message.orgname + ' span').text(),
					content: $root.find('pre:first'),
					timestamp: time,
					date: new Date(time)
				};

			return data;
		}

		function getChatRooms() {
			var $roomList = $roomListBox.find('.' + config.classes.room.el);

			return Array.prototype.slice.call($roomList).reduce(function(memo, roomEl) {
				var $el = $(roomEl),
					room = {
						id: $el.data('rid'),
						name: $el.find('.' + config.classes.room.title).text(),
						pinned: !!$el.find('.' + config.classes.room.pin.on).size(),
						selected: !!$el.hasClass(config.classes.room.selected)
					};

				memo.push(room);
				return memo;
			}, []);
		}

		/**
		 * @constructor
		 */
		function ChatworkExtension() {}

		// エモーティコンを取得して一覧をキャッシュ
		ChatworkExtension._emoticons = Array.prototype.slice.call($emoticonGallery).reduce(function(memo, img) {
			var name = img.src.replace(/(.*?emo_)/, '').replace('.gif', ''),
				token = img.alt;

			memo[name] = token;
			return memo;
		}, {});

		// チャットルーム一覧をオブジェクトの配列化したものを格納し外部へ公開
		ChatworkExtension.chatRooms = [];
		setTimeout(function() {
			ChatworkExtension.chatRooms = getChatRooms();
		}, 500);

		// ################################
		// static methods
		// ################################
		/**
		 * 新しくチャットルームを作成する
		 * @param string name         チャットルーム名
		 * @param string description  チャットルームの詳細説明文
		 * @param array  [members=[]] チャットルームのメンバーのアカウントIDを配列で指定。デフォルトでは自分のみ
		 * @param opts   [opts={}]    チャットルームのオプション、デフォルトでは何もなし
		 * @return $.Deferred チャットルームが作成されたらresolve
		 */
		ChatworkExtension.createRoom = function(name, description, members, opts) {
			members = members || [];
			opts    = opts    || {};

			// TODO: セレクタを設定値化
			$("#_addButton").trigger('click');
			$('[data-cwui-dd-value="addchat"]').click();

			$('#' + config.ids.roomInfo.name).val(name);
			$('#' + config.ids.roomInfo.description).val(description);

			// アイコンの指定があればそれを指定
			if(typeof opts.icon === 'string') {
				$roomInfoIconPreset.each(function() {
					var src = $(this).attr('src');
					if(src.indexOf('heart') >= 0) $(this).click();
				});
			}

			// TODO: メンバー一覧の操作

			// NOTE: 予めキャッシュしているとボタンの要素が取得できない
			var create = $("#_addRoom").prev().find('[aria-label="作成する"]');
			create.trigger('click');

			var deferred = $.Deferred();
			roomObserver.once('add', function(records) {
				var $addedRoom = $(records.target).find('.' + config.classes.room.selected),
					roomInfo   = parseRoom($addedRoom),
					$pin       = $addedRoom.find('.' + config.classes.room.pin.el);

				if(opts.pinned) {
					$pin.click();
					roomInfo.pinned = true;
				}

				deferred.resolve(roomInfo, $addedRoom, records);
			});

			return deferred.promise();
		}
		/**
		 * 現在開いているチャットルームを変更する
		 * @param int      roomId   開くチャットルームのID
		 * @param function callback 指定したチャットルームが開かれたら実行されるコールバック
		 * @return void
		 */
		ChatworkExtension.changeRoom = function(roomId) {
			var $roomList = $roomListBox.find('.' + config.classes.room.el);

			// NOTE: パフォーマンス上breakするためにfor文で書いている
			for(var i = 0, len = $roomList.size(); i < len; i++) {
				var $room = $roomList.eq(i);
				if(+$room.data('rid') === +roomId) {
					$room.click();
					return;
				}
			}
		};
		/**
		 * チャットルームを検索する、なければnullを返す
		 * @param object query 検索に使用したい{プロパティ: 値}
		 * @return any 検索に一致するチャットルームがあればその情報、無ければnull
		 */
		ChatworkExtension.findRoom = function(query) {
			var found = null;
			ChatworkExtension.chatRooms.some(function(room) {
				var ok = true;
				for(var prop in query) {
					if(!query.hasOwnProperty(prop)) continue;
					ok = ok && room[prop] === query[prop];
				}

				if(ok) {
					found = room;
					return true;
				}
			});

			return found;
		}
		/**
		 * チャットワークへ投稿する
		 * チャットワーク記法が使えます。詳しくは http://developer.chatwork.com/ja/messagenotation.html を参照。
		 * ChatworkExtension.notationsにチャットワーク記法ヘルパメソッドを用意しています。合わせてそちらも御覧ください。
		 * @param string body   投稿する本文。
		 * @param int    roomId 投稿するチャットルーム。省略された場合は現在開いているチャットルームへ投稿する
		 * @return void
		 */
		ChatworkExtension.send = function(body, roomId) {
			// roomIdがあったら、そこへ移動しroomIdを空にしてsendを再実行
			// するとチャットを切り替えて現在のチャットルーム（切り替えたルーム）へ投稿できる
			if(typeof roomId !== 'undefined') {
				ChatworkExtension.changeRoom(roomId);
				ChatworkExtension.send(body);

			// 送信を行っている実態部
			} else {
				$chat.val(body);
				$send.click();
			}
		};
		/**
		 * ChatworkExtension.notations.emoticonで使用できる名前と返却される文字列の一覧をコンソールに出力する
		 * @return void
		 */
		ChatworkExtension.emoticons = function() {
			for(var name in ChatworkExtension._emoticons) {
				console.log(name + ' => ' + ChatworkExtension._emoticons[name]);
			}
		};
		ChatworkExtension.ready = function(func) {
			if(ready) {
				func();
			} else {
				roomObserver.once('add', func);
			}
		};
		/**
		 * @param string msg     表示するメッセージ
		 * @param number display 表示する秒数
		 */
		ChatworkExtension.notify = function(msg, display) {
			display = display || 2;

			$('<div></div>')
				.hide()						// いったん非表示にして
				.text(msg)					// テキストを挿入
				.css({						// CSSを設定して
					'margin-bottom': '5px',
					'padding': '10px 20px',
					'background-color': '#fff',
					'border-radius': '5px',
					'box-shadow': '0 0 10px #666',
				})
				.appendTo($notificationBox)	// 通知BOXに挿入
				.fadeIn(500)				// 500m秒でフェードインさせ
				.delay(display * 1000)		// display秒待ち、
				.fadeOut(500, function() { $(this).remove(); });	// フェードアウトしたら自分自身を消去する
		};

		// ################################
		// chatwork notations
		// ################################
		ChatworkExtension.notations = {
			/**
			 * [info]タグを使用する
			 * @param string body         [info]タグの中身
			 * @param string [title=null] [title]タグを設定する場合に使用する。省略された場合titleはつけない
			 * @return string
			 */
			info: function(body, title) {
				var ret = '[info]';
				title = title || null;

				// タイトルがセットされていれば[title]タグを付与
				if(title !== null) ret += '[title]' + title + '[/title]';

				ret += body + '[/info]';
				return ret;
			},
			/**
			 * 水平線([hr]タグ)を使用する
			 * @return string
			 */
			hr: function() {
				return '[hr]';
			},
			/**
			 * [To:{account_id}]タグを使用する
			 * @param object accounts アカウントID: 表示名というオブジェクトでToを複数指定する
			 * @param string suffix   人名につける敬称。省略するとさんを付与する
			 * @return string
			 */
			to: function(accounts, suffix) {
				suffix = suffix || 'さん';

				var ret = '';
				for(var accountId in accounts) {
					var name = accounts[accountId] || '';
					if(!accounts.hasOwnProperty(accountId)) continue;

					ret += '[To:' + accountId + '] ' + name + suffix + "\n";
				}

				return ret;
			},
			/**
			 * [rp]タグを使用する
			 * @param number accountId 返信するアカウントID
			 * @param number roomId    返信するチャットルームのID
			 * @param number messageId 返信するメッセージのID
			 */
			reply: function(accountId, roomId, messageId) {
				var reply = '[rp aid=' + accountId + ' to=' + roomId + '-' + messageId + ']';
				return reply;
			},
			/**
			 * [qt]タグを使用する
			 * @param number accountId        引用元のアカウントID
			 * @param string body             引用するテキスト
			 * @param number [timestamp=null] 囲繞するメッセージのUNIXタイムスタンプ。省略された場合は時間を表示しない
			 * @return string
			 */
			quote: function(accountId, body, timestamp) {
				timestamp = timestamp || null;
				var quote = '[qt]';

				if(timestamp !== null) {
					quote += '[qtmeta aid=' + accountId + ' time=' + timestamp + ']';
				} else {
					quote += '[qtmeta aid=' + accountId + ']';
				}

				quote += body + '[/qt]';
				return quote;
			},
			/**
			 * [picon]タグ（アカウントのアイコン表示）を使用する
			 * @param number accountId 表示するアイコンのアカウントID
			 * @return string
			 */
			picon: function(accountId) {
				return '[picon:' + accountId + ']';
			},
			/**
			 * [piconname]タグ（アカウントのアイコンと名前表示）を使用する
			 * @param number accountId 表示するアイコンのアカウントID
			 * @return string
			 */
			piconname: function(accountId) {
				return '[piconname:' + accountId + ']';
			},
			/**
			 * エモーティコンを使用する
			 * @param string name   エモーティコンの名前。画像名がemo_xxx.gifとなっていて、xxxの部分を指定する
			 * @param number repeat 繰り返し回数。デフォルトは1
			 */
			emoticon: function(name, repeat) {
				repeat = repeat || 1;
				return new Array(repeat + 1).join(ChatworkExtension._emoticons[name]);
			}
		};

		/**
		 * 監視を行うメソッド一覧
		 * @param string type change,clickなどを考え中。とりあえずchangeのみ受付
		 */
		var observers = {
			room: function(type, action) {
				// changeはDOMイベントじゃないのでカスタムイベントを作成する
				if(type === 'change') {
					chatObserver.on('add', action);
				}
			}
		};

		// ################################
		// instance methods
		// ################################
		ChatworkExtension.prototype = {
			/**
			 * 各アクションに対してフックを仕掛ける
			 * アクションの指定は`[eventType]:[target]`という書式で指定する。
			 * 例えばclick:messageのような感じ。
			 * @param string   event  上記の書式の文字列
			 * @param function action 指定したアクションが起こった場合のイベントハンドラ
			 */
			hook: function(event, action) {
				var events = event.split(':'),
					type   = events[0],
					target = events[1];

				observers[target](type, action);
			},
			/**
			 * マウスでメッセージをドラッグしたときに出るメニューを拡張
			 * @method addQuoteMenu
			 * @param string   name   メニューに表示する文字列
			 * @param function action クリックされた際のイベント
			 * @param string   id     メニューに設定するID。省略すると自動で一意なIDが振られる
			 * @return ChatworkExtension 自分自身のインスタンスを返す
			 */
			addQuoteMenu: function(name, action, id) {

				return this;
			},
			/**
			 * １つ１つのメッセージに表示するメニューを拡張する
			 * @param string   name   メニューに表示する文字列
			 * @param function action クリックされた際のイベント
			 * @param string   [icon] メニューに設定するアイコンのクラス名。省略するとなし
			 */
			addMessageMenu: function(name, action, icon) {
				var $menu = generateMessageMenu(name, icon);

				$menu.on('click', function() {
					var parsed = parseMessage($(this));
					action.apply(this, [parsed]);
				});

				$(document).on('mousemove', '.' + config.classes.message.el, function() {
					if($(this).data('fav-menu-added')) return false;

					$(this).find('ul.actionNav').prepend($menu.clone(true));
					$(this).data('fav-menu-added', true);
				});
			}
		};

		return ChatworkExtension;
	}());

	global.ChatworkExtension = ChatworkExtension;
}(this, this.jQuery));
