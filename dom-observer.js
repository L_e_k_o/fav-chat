(function(global, $, undefined) {

	/**
	 * @class DOMObserver
	 */
	var DOMObserver = (function() {
		function callbackStructure() {
			return { add: [], remove: [] };
		}
		function DOMObserver(el, opts) {
			// ユーザ指定のコールバックを発火する用のコールバック
			var fire = function(records) {
				if(records.length <= 0) return;
				records = records[0];

				// 追加されたノードが存在すればaddイベントを発火
				if(records.addedNodes.length > 0)
					this.trigger('add', [records], records.target);

				// 削除されたノードが存在すればremoveイベントを発火
				if(records.removedNodes.length > 0)
					this.trigger('remove', [records], records.target);
			};

			// コールバックを初期化
			this.callbacks = callbackStructure();
			this.callbacksOnce = callbackStructure();

			// 変更を検知した際のコールバックを設定
			this.observer = new MutationObserver(fire.bind(this));

			// 監視設定
			this.observer.observe(el, opts);
		}

		DOMObserver.prototype = {
			/**
			 * イベントにコールバックを設定する
			 * @param string   type           指定するイベント名
			 * @param function callback       指定するコールバック
			 * @param any      [context=null] コールバックに指定するコンテキスト。省略するとnullが指定される
			 * @return DOMObserver 自分自身のインスタンスを返す
			 */
			on: function(type, callback, context) {
				context = context || null;
				this.callbacks[type].push(callback.bind(context));
				return this;
			},
			/**
			 * イベントに一度だけ実行されるコールバックを設定する
			 * @param string   type           指定するイベント名
			 * @param function callback       指定するコールバック
			 * @param any      [context=null] コールバックに指定するコンテキスト。省略するとnullが指定される
			 * @return DOMObserver 自分自身のインスタンスを返す
			 */
			once: function(type, callback, context) {
				context = context || null;
				this.callbacksOnce[type].push(callback.bind(context));
				return this;
			},
			/**
			 * イベント、コールバックの購読を解除する
			 * @param string   [type]     購読を解除するイベントタイプ。省略されたら全てのイベントの購読を解除する
			 * @param function [callback] 購読を解除するコールバック。省略されたら指定されたイベントタイプの全てのコールバックを解除する
			 * @return DOMObserver 自分自身のインスタンスを返す
			 */
			off: function(type, callback) {
				// typeがなければ全て解除
				if(typeof type === 'undefined') {
					this.callbacks = callbackStructure();
					this.callbacksOnce = callbackStructure();
				} else {
					// callbackが指定されていなければ全て解除
					if(typeof callback === 'undefined') {
						this.callbacks[type] = [];
						this.callbacksOnce[type] = [];

					// callbackが指定されていたらそれだけ消去
					} else {
						this.callbacks[type] = this.callbacks[type].filter(function(func) {
							return func !== callback;
						});
						this.callbacksOnce[type] = this.callbacksOnce[type].filter(function(func) {
							return func !== callback;
						});
					}
				}
			},
			/**
			 * イベントに設定されたコールバックを実行する
			 * @param string type    発火するイベント名
			 * @param array  args    コールバックへ渡す引数の配列
			 * @param any    context コールバックにバインドするコンテキスト。省略されたらnullが指定される
			 * @return DOMObserver 自分自身のインスタンスを返す
			 */
			trigger: function(type, args, context) {
				context = context || null;

				this.callbacks[type].concat(this.callbacksOnce[type]).forEach(function(func) {
					func.apply(context, args);
				});

				// onceで登録されているものは1度実行されたら消去
				this.callbacksOnce[type] = [];
			},
			/**
			 * DOMの監視を解除し、全てのコールバックを解除する
			 * @return void
			 */
			release: function() {
				this.observer.disconnect();

				// NOTE: 明示的にGCに回収してもらう
				this.observer = null;
				this.callbacks = null;
				this.callbacksOnce = null;
			}
		};

		return DOMObserver;
	}());

	global.DOMObserver = DOMObserver;
}(this, this.jQuery));
