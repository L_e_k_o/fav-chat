# fav-chat

チャットワークの発言をふぁぼります。

## ふぁぼ？
Twitterの「お気に入り」のようなものと捉えていただけると幸いです。

![img](https://dl.dropboxusercontent.com/u/48987130/public_images/fav-chat/favorite.png)

このように、メッセージの下にあるメニューに**ふぁぼ**という項目が追加されています。  
これをクリックすると、

![img](https://dl.dropboxusercontent.com/u/48987130/public_images/fav-chat/favorite-result.png)

このように、引用文形式で自分のふぁぼチャットへ投稿されます。

## 免責事項
バグ等によって自分のふぁぼチャット以外の場所へ引用されてしまっても責任は負いかねます。
ご理解・ご了承の上、ご使用ください。

## インストール・導入
等プラグインは`Chrome拡張`として作成されています。  
そのため、Google Chrome以外では使用することが出来ません。予めご了承下さい。

### 1. ダウンロード
このプロジェクトをzipで落とすなりcloneするなりして、ローカルにダウンロードして下さい。

次に、Google Chromeの「ウィンドウ」メニューの中にある「拡張機能」を選択して下さい。

![img](https://dl.dropboxusercontent.com/u/48987130/public_images/fav-chat/step01.png)

すると拡張機能一覧画面が出てくるので、「パッケージ化されていない拡張機能を読み込む...」をクリック。  
ファイルを選択する画面になるので、1でダウンロードしたfav-chatを選択します。

![img](https://dl.dropboxusercontent.com/u/48987130/public_images/fav-chat/step02.png)

下記のように、一覧に表示されていれば準備完了です。  

![img](https://dl.dropboxusercontent.com/u/48987130/public_images/fav-chat/step03.png)

チャットワークでも素敵なふぁぼライフをお楽しみ下さい。
