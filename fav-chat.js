(function(global, $, undefined) {
	'use strict';

	// constants
	var FAV_ROOM_NAME = 'ふぁぼ';
	var FAV_ROOM_DESCRIPTION = 'あなたのふぁぼ一覧です。';
	var LOCALSTORAGE_KEY = 'favChatFavRoomId';

	var favChat = new ChatworkExtension();

	function getLocal() {
		return localStorage.getItem(LOCALSTORAGE_KEY);
	}
	function setLocal(val) {
		localStorage.setItem(LOCALSTORAGE_KEY, val);
	}

	ChatworkExtension.ready(function() {
		// ルーム一覧の中に「ふぁぼ」というチャットがあるか
		var favRoom = ChatworkExtension.findRoom({ name: FAV_ROOM_NAME }),
			savedLocalStorage = (getLocal() !== null);

		// ローカルストレージにふぁぼルームのIDが無ければ
		// ふぁぼルームを作成し、そのルームIDをローカルストレージにセットする
		if(!savedLocalStorage) {
			// NOTE: 読み込み完了を待って、更にもう少し待たないとモーダルが作動しない
			setTimeout(function() {
				ChatworkExtension.createRoom(FAV_ROOM_NAME, FAV_ROOM_DESCRIPTION, [], { pinned: true, icon: 'heart' })
					.done(function(roomInfo) {
						setLocal(roomInfo.rid);
					});
			}, 2000);
		}

		// 各メッセージのメニューに「ふぁぼ」を追加し、クリックされた投稿をふぁぼチャットに投げつける
		favChat.addMessageMenu('ふぁぼ', function(data) {
			var $currentRoom = $('#_roomListItems li._roomSelected'),
				body = $(data.content).text(),
				quoted = ChatworkExtension.notations.quote(data.aid, body, data.timestamp);

			// ChatworkExtension.changeRoom(getLocal());
			ChatworkExtension.send(quoted, getLocal());

			// NOTE: 戻す
			$currentRoom.trigger('click');

			ChatworkExtension.notify(data.name + "さんの発言をふぁぼりました");
		}, 'icoFontAddBtn');

		console.log('DEBUG: fav-chat is working...');
	});
}(this, this.jQuery));
